// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

let str = ["the", "quick", "brown", "fox"]
function string5(str) {
    str.length > 0 ? str = str.join(' ') : str = [] 
    return str
}

let result = string5(str)
console.log(result)
module.exports = string5


