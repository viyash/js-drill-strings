// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}



let obj = {"first_name": "JoHN", "middle_name": "doe","last_name": "SMith"}
function string4(obj) {
    let mName = obj['middle_name'] ? obj['middle_name']+' ' : ''
    let name = obj['first_name'] + ' ' + mName + obj['last_name']
    str = name.split(' ')
    for(let index =0 ; index < str.length; index++){
        str[index] = str[index].charAt(0).toUpperCase() + str[index].toLowerCase().slice(1)
    }
    return str.join(' ')
}

let result = string4(obj)
console.log(result)
module.exports = string4


