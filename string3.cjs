// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.


let str = "10/01/2021"
function string3(str) {
    str = str.split('/')
    return str[1]
}

let result = string3(str)
console.log(result)
module.exports = string3


